exec : q4.o main.o
        gcc -o exec q4.o main.o

q4.h : q4.o main.o
        gcc -h q4.h q4.o main.o

q4.o : q4.c
        gcc -o q4.o -c q4.c -Wall -O
main.o: main.c q4.h
    gcc -o main.o -c main.c -Wall -O
     
clean :
    rm - f core *.o
