# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: SEKKOURI ALAOUI, Etienne, etienne.sekkourialaou@etu.univ-lille.fr

- Nom, Prénom, email: ___

## Question 1

Réponse Le processus peut écrire pour les utilisateurs du groupe ubuntu, mais pas pour toto ?

## Question 2 

Réponse Pour un dossier, cela signifie qu'on peut y entrer
On ne peut pas rentrer dans le dossier, "permission denied"
La meme chose car pour fonctionner , ls doit ouvrir le dossier

## Question 3

Réponseu id:1001
	egid=1000
	gid=1000
	euid=1001
Le fichier n'est pas lisible par l'utilisateur toto.
Apèrs le changement du flag :
	uid:1001
	egid=1000
	gid=1000
	euid=1000
Le fichier est maintenant lisible pour l'utilisateur toto.
## Question 4

Réponse euid:1001
	egid:1000

## Question 5

Réponse chfn permet de modifier les informations personnel d'un utilisateur.
Les permissions sont pour un ficheir. L'utilisateur root peut écrire , lire et grace à set-user-id lire. Le groupe peut lire et executer, les autres peuvent lire et executer.
## Question 6

Réponse Les mots de passe sont situés dans le fichier /etc/shadow
Ce fichier est uniquement accéssible en lecture et écriture par root, et peut etre lu par le groupe root.
Comme ce fichier, contient les mots de passe en clair il doit etre limité d'accés.
Or, passwd contient une version cryptée qui permet à tous les utilisateurs de vérifier les droits des différents fichiers. Il doit donc etre accéssible.

## Question 7



Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








